var express = require("express");
var app	= express();
var request = require ("request");
app.set("view engine", "ejs");

app.get("/", function(req, res){ 
	res.render("search");
});
app.get("/results",function(req,res){ 
	var query = req.query.search;
	var url = "http://openweathermap.org/data/2.5/find?q=" + query +"&appid=b6907d289e10d714a6e88b30761fae22";
    request(url, function(error,response,body){
        if(!error && response.statusCode==200){
            var data=JSON.parse(body);
            res.render("results",{data:data});
        }
    });
});

app.listen(3000, function () {
  console.log("Server Started!")
});